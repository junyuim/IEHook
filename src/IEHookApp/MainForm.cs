﻿using Application.Presentation.Windows.IEHook;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace IEHookApp
{
    public partial class MainForm : Form
    {
        private IEBrowserHelperObjectRegister register;

        public MainForm()
        {
            InitializeComponent();
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            this.register = new IEBrowserHelperObjectRegister();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.register.Register();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.register.Unregister();
        }
    }
}
