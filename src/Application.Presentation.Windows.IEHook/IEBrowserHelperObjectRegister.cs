﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Application.Presentation.Windows.IEHook
{
    public class IEBrowserHelperObjectRegister : IDisposable
    {
        private bool registered;

        public void Dispose()
        {
            this.Unregister();
        }

        public void Register()
        {
            if (this.registered)
            {
                return;
            }

            var codebase = Assembly.GetExecutingAssembly().CodeBase.Remove(0, 8);

            Process.Start(new ProcessStartInfo()
            {
                FileName = @"C:\Windows\Microsoft.net\Framework\v2.0.50727\regasm.exe",
                Arguments = $"/codebase {codebase}",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            });

            registered = true;
        }

        public void Unregister()
        {
            if (!this.registered)
            {
                return;
            }

            var codebase = Assembly.GetExecutingAssembly().CodeBase.Remove(0, 8);

            Process.Start(new ProcessStartInfo()
            {
                FileName = @"C:\Windows\Microsoft.net\Framework\v2.0.50727\regasm.exe",
                Arguments = $"/unregister {codebase}",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            });
        }
    }
}
